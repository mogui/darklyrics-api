from toapi import Item, Css
from flask import request

def parse_type(url):
    if url.find('http://') >= 0:
        return "band"
    elif url.find('#') >= 0:
        return "song"
    else: 
        return "album"

class Search(Item):
    title = Css('h2 a')
    original_url = Css('h2 a', attr='href')
    url = Css('h2 a', attr='href')
    type = Css('h2 a', attr='href')

    def clean_url(self, url):
        type = parse_type(url)
        if type == "band":
            u = url.replace("http://www.darklyrics.com", "").replace(".html", "")
        elif type == "album":
            u = url.replace('lyrics', '').replace('.html', '')
        else:
            u = url.replace('lyrics', '').replace('.html', '').replace('#', '/')
        return request.base_url.replace('/search', '') + u

    def clean_type(self, url):
        return parse_type(url)


    class Meta:
        source = Css('div.sen')
        route = {'/search?q=:query': '/search?q=:query',
                '/search?q=:query&p=:page': '/search?q=:query&p=:page'}
