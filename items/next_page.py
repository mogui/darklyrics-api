from toapi import Item, Css
from functools import reduce

class Pages(Item):
    next = Css('div.search a')
    last = Css('div.search a')

    def clean_next(self, pages):
        if len(pages) == 0:
          return None
        next_p = None
        for e in pages:
            if e.text == 'Next':
                next_p = e.get("href")[-1:]
                break
        if next_p is None:
            return None

        return int(next_p)

    def clean_last(self, pages):
        if len(pages) == 0:
          return 1
        b = list(filter(lambda x: x.text.isdigit(), pages))
        last = reduce((lambda x, y: x if int(x.text) > int(y.text) else y), b)
        return int(last.text) - 1 
        


    class Meta:
        source = None #Css('div.search a')
        route = {'/search?q=:query': '/search?q=:query',
                '/search?q=:query&p=:page': '/search?q=:query&p=:page'}

