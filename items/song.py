from toapi import Item, Css, XPath
from flask import request
from lxml.etree import _Element


class Song(Item):
    title = Css('a')
    lyrics = XPath('//div[@class="lyrics"]/node()')

    def clean_title(self, title):
        song_n = request.path.split('/')[-1]
        tit = list(filter(lambda e: e.get('name') == song_n, title))[0]
        return tit.text.replace('%s. ' % song_n, '')

    def clean_lyrics(self, lyrics):
        song_n = request.path.split('/')[-1]
        start = 0
        end = len(lyrics)
        for i, e in enumerate(lyrics):
            if isinstance(e, _Element) and e.tag == 'h3':
                if start != 0:
                    end = i
                    break
                else:
                    if e.find('a').text.startswith(song_n):
                        start = i
        raw = lyrics[start+1:end]
        
        # actual_lyric = list(filter(lambda e: isinstance(e, str), lyrics[start:end]))
        raw = list(filter(lambda e: isinstance(e, str) or e.tag != 'br', raw))
        print(raw)
        actual_lyric = [e if isinstance(e, str) else e.text for e in raw]
        return "".join(actual_lyric)


    class Meta:
        source = Css('.lyrics')
        route = {'/:band/:album/:song': 'lyrics/:band/:album.html#:song'}