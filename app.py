from toapi import Api
from items.search import Search
from items.next_page import Pages
from items.song import Song
from settings import MySettings

api = Api('http://www.darklyrics.com/', settings=MySettings)
api.register(Search)
api.register(Pages)
api.register(Song)

if __name__ == '__main__':
    api.serve()
