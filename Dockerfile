FROM python:3.5-jessie
MAINTAINER mogui <mogui83@gmail.com>

ENV APP_PATH /app
RUN mkdir -p $APP_PATH
WORKDIR $APP_PATH

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

VOLUME ["/app/storage"]
COPY . /app

EXPOSE 5555

CMD gunicorn -b 0.0.0.0:5555 -w 4 wsgi:app
